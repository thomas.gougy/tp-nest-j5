# TP du 03/02 - Groupe 2

## Membres du groupe :
- Nicolas GERVAUX
- Thomas GOUGY


## Context :

La médiathèque Saint-Bernard lance un appel d'offres pour réaliser son nouveau système de gestion. Actuellement les membres du personnel tiennes la liste de leurs média et des adhérents sur un registre en papier. L'équipe aimerait informatiser cette gestion. Cette informatisation aura pour objectif de faire gagner du temps à l'équipe et aux adhérents.



## Justification des choix

### BDD

![logo de postgres](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/langfr-220px-Postgresql_elephant.svg.png)

Nous sommes partis sur du PostgreSQL. Dans un premier temps, volonté d'avoir une BDD axée relationnelle. Postgre plutôt que MariaDB dans un soucis de scalabilité dans le temps.
nous nous sommes basé sur les connaissance de chacun et avons défini les necessité technique 

![Choix de la base de données](./images/choixDB.png)

Ce qui fais que nous avons choisi de construire notre Base de données comme ceci

![Modelisation de la base](./images/modelisationBdd.png)

### Back-end

#### Micro-service
le projet de base est plus facilement réalisable en Monolithe mais la Mediathèque ayant pour projet de s'agrandire, nous somme arti sur une aproche en micro service.

##### les services 
notre back end sera divisé en 5 micro-services

**user-service**:
le user service sera le service qui fa soccuper de la securisation de l'application il servira pour la creation et l'authentification du personnel de la mediathèque

**Adherent-service**:
le service Adherent servira a la creation des nouveau adherant et a la gestion des different niveau d'abonnement 

**emprunt-service**:
le service d'emprunt va s'occuper de tout la partie emprunt des different média de la médiathèque.

**retard-service**
le service retard s'occupera de toute la gestion des retard de rendu et grace a cron indiquera au notification service l'envoi du mail

**notification-service**
le service de notification est le service qui s'occupera de l'envoi des different mail que ce soit pour les personne de la mediatheque ou les adhérent 
#### NestJS

![logo nest js](https://cdn.icon-icons.com/icons2/2699/PNG/512/nestjs_logo_icon_168087.png)

le client a expressement demander que le back soit réaliser en NestJs, Car il existe deja des élément qui on été produit en nest js dans l'infrastructure de la mediathèque et celle ci ne souhaite pas s'éparpiller sur differente technologie.

### Pub-Sub

#### Redis

![Logo de redis](https://blogiestools.com/wp-content/uploads/2021/02/Redis-review.jpg)

Pour la mise en place du Pub Sub nous avons decidé de prendre redis. 
car la base de connaisance de l'équipe est uniquement presente sur cette téchnologie 
mais nous avons aussi un nodes molude prévu pour la gestion du Pub Sub de redis :

[Node module réalisé par Nathanael Elisabeth](https://www.npmjs.com/package/@mistercoookie/nestjs-redis-pub-sub)

ce qui fais que la partie back va se présenter comme ceci :

![visuel des services](./images/services.png)
### Front

#### Angular
![logo angular](https://angular.io/assets/images/logos/angularjs/AngularJS-Shield.svg)

Pour la partie fornt nous sommes parti sur Angular 
Car c'etait la technologie la mieux maitriser par l'équipe ce que permet un gain de temps lors du dévellopement 

![diagramme choix du front](./images/front.png)


### Gestion abonnement

Afin de vérifier si un adhérent doit changer d'abonnement, une tâche CRON sera exécuté la nuit
pour vérifier pour chaque adhérent si les conditions sont présente pour un changement. Si oui,
changement d'abonnement pour l'adhérent avec envoie d'un mail pour le prévenir via le service Notification.

### Gestion retard

Afin de vérifier les délais pour rendre les livres, une tâche CRON sera exécuté la nuit pour vérifier pour chaque emprunt si un mail doit être envoyé (1 semaine avant date finale, tout les jours après date finale, etc). De plus, en cas de retard, cela est inscrit en BDD dans la table retard + isChecked est passé à true dans la table emprunt + envoie d'un mail à l'équipe de la médiathèque.

### réalisation

pour la réalisation de ce projet nous avons fais un diagramme de Gantt afin d'avoir une idée du temps de développement du projet 

![diagramme de gantt](./images/gantt.png)


ce qui donnerait une vue d'ensemble se rapprochant de ceci

![vue d'ensemble](./images/ensemble.png)
