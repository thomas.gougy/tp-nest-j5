import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { EmpruntController } from './emprunt/emprunt.controller';
import { EmpruntService } from './emprunt/emprunt.service';
import { EmpruntModule } from './emprunt/emprunt.module';


@Module({
  imports: [ConfigModule.forRoot(),DatabaseModule, EmpruntModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
