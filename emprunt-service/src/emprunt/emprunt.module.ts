import { Module } from '@nestjs/common';
import { EmpruntService } from './emprunt.service';
import { EmpruntController } from './emprunt.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/database/entities/user.entity';
import { Abonnement } from 'src/database/entities/abonnement.entity';
import { AbonnementType } from 'src/database/entities/abonnementType.entity';
import { Adherent } from 'src/database/entities/adherent.entity';
import { Media } from 'src/database/entities/media.entity';
import { Emprunt } from 'src/database/entities/emprunt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User,Abonnement,AbonnementType,Adherent,Media,Emprunt])],
  providers: [EmpruntService],
  controllers: [EmpruntController]
})
export class EmpruntModule {}
