import { IsNotEmpty, IsString } from "class-validator";

export class AdherentDto {
    @IsString()
    @IsNotEmpty()
    adherentId: string;
}