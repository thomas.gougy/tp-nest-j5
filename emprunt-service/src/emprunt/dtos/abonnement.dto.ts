import { IsNotEmpty, IsString } from "class-validator";

export class AbonnementDto {
    @IsString()
    @IsNotEmpty()
    abonnementTypeId: string
}