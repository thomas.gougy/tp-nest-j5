import { IsString } from "class-validator";

export class MediaDto {
    @IsString()
    id: string

    @IsString()
    name: string

    @IsString()
    type: string

}