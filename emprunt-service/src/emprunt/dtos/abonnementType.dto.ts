import { IsInt } from "class-validator";

export class abonnementTypeDto {
    @IsInt()
    nbEmpruntMax: number

    @IsInt()
    tempsMax: number
}