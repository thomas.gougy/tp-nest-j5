import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { User } from 'src/database/entities/user.entity';
import { ServiceAuthGuard } from 'src/guard/auth.guard';
import { EmpruntDto } from './dtos/emprunt.dto';
import { EmpruntService } from './emprunt.service';

@UseGuards(ServiceAuthGuard)
@Controller('emprunt')
export class EmpruntController {

    constructor(private empruntService: EmpruntService) {}

    @Get('/media/:mediaId')
    getOneMedia(@Req() request: {user: User}, @Param('mediaId') mediaId: string) {
        return this.empruntService.getOneMedia(request.user,mediaId)
    }

    @Get('/media')
    getAllMediaAvailable(@Req() request: {user: User}) {
        return this.empruntService.getAll(request.user)
    }

    @Post('/:id/media/:mediaId')
    createEmprunt(@Req() request: {user: User},@Body()
        emprunt: EmpruntDto,
        @Param('id') adherent:string,
        @Param('mediaId') media:string
        ): Promise<EmpruntDto>
         {
        return this.empruntService.createEmprunt(emprunt,adherent,media)
    }

    
}
