import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Abonnement } from 'src/database/entities/abonnement.entity';
import { AbonnementType } from 'src/database/entities/abonnementType.entity';
import { Adherent } from 'src/database/entities/adherent.entity';
import { Emprunt } from 'src/database/entities/emprunt.entity';
import { Media } from 'src/database/entities/media.entity';
import { User } from 'src/database/entities/user.entity';
import { IsNull, Repository } from 'typeorm';
import { AdherentDto } from './dtos/adherent.dto';
import { EmpruntDto } from './dtos/emprunt.dto';
import { MediaDto } from './dtos/media.dto';

@Injectable()
export class EmpruntService {

    constructor(
        @InjectRepository(Media)
        private mediaRepository: Repository<Media>
    ) {}


    async getOneMedia(user:User,mediaId:string): Promise<Media> {
        const media = await Media.findOneBy({ id:mediaId })

        if (media == null){
            throw new NotFoundException('media not found');
        }
        
        return media
    }

    async getAll(user:User): Promise<Media[]> {
        const medias = await Media.find()

        return medias
    }

    async createEmprunt(empruntData: EmpruntDto, adherentData: string, mediaData: string): Promise<Emprunt> {
        const emprunt = new Emprunt()

        //On recherche l'abonnement de l'adhérent
        const abonnement = await Abonnement.findOne({
            relations: ['adherent','abonnementType'],
            where: {
              adherent:{id: adherentData},
              dateFin: IsNull() 
            }
        })

        //Si on ne trouve pas d'abonnement, c'est que l'adhérent n'a plus d'abonnement (exemple : ne paye plus)
        if (abonnement == null) {
            throw new ForbiddenException("Vous n'avez pas d'abonnement")
        }

        /*TODO :
        Contrôler le nombre d'emprunts actuel de l'adhérent et comparer à son nombre maximum */

        //On récupère le temps maximum d'emprun pour définir la dateFinale de rendu :
        const tempsMax = abonnement.abonnementType.tempsMax
        const now = new Date()
         now.setMonth(now.getMonth() + tempsMax)
        emprunt.dateFinale= now


        //On associe le média à l'emprunt
        const media = await Media.findOneBy({id:mediaData })
        emprunt.media = media

        console.log(emprunt)
        return emprunt.save();
    }
}
