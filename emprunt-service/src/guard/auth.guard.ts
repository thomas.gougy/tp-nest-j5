import { RedisService } from "@mistercoookie/nestjs-redis-pub-sub";
import { Injectable, Logger, Req } from "@nestjs/common";
import {AuthGuard, PassportStrategy} from "@nestjs/passport";
import { Strategy } from "passport-jwt";

@Injectable()
export class ServiceAuthGuard extends AuthGuard('service') {}

@Injectable()
export class ServiceAuthStrategy extends PassportStrategy(Strategy, 'service') {
    private logger: Logger = new Logger('ServiceAuthStrategy')
    constructor() {
        super()
    }

    async validate(@Req() request) {
        if (request.headers.hasOwnProperty('authorization') == false) {
            return false
        }

        // Authorization : Bearer header.payload.sign
        const tokenParts = request.headers['authorization'].split(' ')
        if(tokenParts.length != 2){
            return false
        }

        const user = await RedisService.publishWithAnswer<string, object>(
            'g2-validation',
            tokenParts[1]
        )
        return user
    }
}
