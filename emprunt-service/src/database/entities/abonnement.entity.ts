import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, BaseEntity, OneToMany } from "typeorm"
import { AbonnementType } from "./abonnementType.entity"
import { Adherent } from "./adherent.entity"

@Entity()
export class Abonnement extends BaseEntity{
    @PrimaryGeneratedColumn('uuid')
    id: string

    @ManyToOne(()=>AbonnementType, (type)=> type, {eager:true})
    abonnementType: AbonnementType

    @Column('date',{default: new Date()})
    dateDebut: Date

    @Column('date',{nullable:true})
    dateFin: Date

    @ManyToOne(()=> Adherent, (adherent)=> adherent.abonnements)
    adherent: Adherent

}