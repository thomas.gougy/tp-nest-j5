import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from "typeorm"
import { Abonnement } from "./abonnement.entity"

@Entity()
export class AbonnementType extends BaseEntity{

    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column('varchar')
    name: string

    @Column('int')
    nbEmpruntMax

    @Column('int')
    tempsMax: number

    @Column('int', {nullable: true})
    conditionUp: number

    @Column('int', {nullable: true})
    conditionDown: number

    @OneToMany(()=> Abonnement, (abonnement)=> abonnement.abonnementType)
    abonnements: Abonnement[]
}