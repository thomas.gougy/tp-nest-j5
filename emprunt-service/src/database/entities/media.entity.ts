import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Emprunt } from "./emprunt.entity";

@Entity()
export class Media extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string
    
    @Column('varchar')
    name: string

    @Column('varchar')
    type: string

    @Column('boolean', {default:true})
    isAvailable: boolean

    @OneToMany(() => Emprunt, (emprunt) => emprunt.media)
    emprunts: Emprunt[];
}