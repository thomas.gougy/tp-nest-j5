import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Abonnement } from "./entities/abonnement.entity";
import { AbonnementType } from "./entities/abonnementType.entity";
import { Adherent } from "./entities/adherent.entity";
import { Emprunt } from "./entities/emprunt.entity";
import { Media } from "./entities/media.entity";
import { User } from "./entities/user.entity";

@Module({
    // Les imports qui seront utilisé dans le modules
    imports: [
        // Initialisation du module typeorm
        TypeOrmModule.forRootAsync({
            // ConfigModule.forRoot()
            imports: [ConfigModule.forRoot()],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('DB_HOST'),
                port: configService.get('DB_PORT'),
                username: configService.get('DB_USER'),
                password: configService.get('DB_PASSWORD'),
                database: configService.get('DB_NAME'),
                entities: [User,Abonnement,AbonnementType,Adherent,Emprunt,Media],
                synchronize: true
            })
        })
    ]
})
export class DatabaseModule {}
