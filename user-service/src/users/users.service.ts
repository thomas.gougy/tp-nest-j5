import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { UserRegisterDTO } from './dtos/userRegister.dto';
import { UserLoginDTO } from './dtos/userLogin.dto';
import { User } from 'src/database/entities/user.entity';
import { hash, compare } from 'bcrypt'
import { JwtService } from '@nestjs/jwt';
import { RedisService } from '@mistercoookie/nestjs-redis-pub-sub';

@Injectable()
export class UsersService {
    private logger: Logger = new Logger("UsersService")
    constructor(
        private jwtService: JwtService,
    ) {}

    async verifyToken(token: string) {
        const payload = this.jwtService.verify(token)
        const user = await User.findOne({where: {id: payload.userId}, select: ['email', 'firstname', 'lastname', 'id']})
        return user
    }

    async register(userRegisterDTO: UserRegisterDTO) {
        const user = new User()
        
        user.email = userRegisterDTO.email
        user.lastname = userRegisterDTO.lastname
        user.firstname = userRegisterDTO.firstname

        user.password = await hash(userRegisterDTO.password, 10)
        
        await user.save()

        RedisService.publish<{email: string, firstname: string, lastname: string}>(
            'g2-register',
            {
            email: user.email,
            firstname: user.firstname,
            lastname: user.lastname
            }
        )
        
        return user;
    }

    async login(userLoginDTO: UserLoginDTO) {
        const user = await User.findOne({where: {email: userLoginDTO.email}})
        
        if (user == null) {
            throw new BadRequestException("L'email ou le mot de passe fournit n'est pas correct")
        }
        
        const isPasswordMatching = await compare(userLoginDTO.password, user.password)

        if (isPasswordMatching == false) {
            throw new BadRequestException("L'email ou le mot de passe fournit n'est pas correct")
        }
        const token = this.jwtService.sign({
            userId: user.id
        })

        RedisService.publish<{firstname: string,answer: string}>(
            'ttjn-login',
            {
                firstname:user.firstname,
                answer: 'Vous êtes bien connecté(e)'
            }
        )

        return { token: token }
    }
}
