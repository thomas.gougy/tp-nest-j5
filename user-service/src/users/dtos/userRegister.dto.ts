import { Exclude } from "class-transformer";
import { IsNotEmpty, IsString, MinLength } from "class-validator";


export class UserRegisterDTO {

    @IsString()
    @IsNotEmpty()
    firstname: string;

    @IsString()
    @IsNotEmpty()
    lastname: string;

    @IsString()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    password: string;
}