import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";

@Module({
    // Les imports qui seront utilisé dans le modules
    imports: [
        // Initialisation du module typeorm
        TypeOrmModule.forRootAsync({
            // ConfigModule.forRoot()
            imports: [ConfigModule.forRoot()],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('DB_HOST'),
                port: configService.get('DB_PORT'),
                username: configService.get('DB_USER'),
                password: configService.get('DB_PASSWORD'),
                database: configService.get('DB_NAME'),
                entities: [User],
                synchronize: true
            })
        })
    ]
})
export class DatabaseModule {}
