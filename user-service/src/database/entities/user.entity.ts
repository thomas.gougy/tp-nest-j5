import { Exclude } from "class-transformer";
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column('varchar', { unique: true })
    email: string

    @Exclude()
    @Column('varchar')
    password: string

    @Column('varchar')
    firstname: string
    
    @Column('varchar')
    lastname: string
}