import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { AuthGuard, PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt'
import { User } from "src/database/entities/user.entity";

@Injectable()
export class JwtAuthenticationGuard extends AuthGuard('jwt') {}

@Injectable()
export class JwtAuthenticationStrategy extends PassportStrategy(Strategy) {

    constructor(
        private configService: ConfigService
    ){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get('JWT_SECRET')
        })
    }

    async validate(payload: {tokenId: string}) {
        const user = User.findOne({where: { id: payload.tokenId }})
        
        if (user == null) {
           new UnauthorizedException('Vous devez être authentifier') 
        }

        return user
    }
}

export interface RequestWithUser extends Request {
    user: User
}