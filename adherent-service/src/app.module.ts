import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {DatabaseModule} from "./datababe/database.module";
import {ConfigModule} from "@nestjs/config";
import { AdherentModule } from './adherent/adherent.module';
import { AbonnementModule } from './abonnement/abonnement.module';

@Module({
  imports: [ConfigModule.forRoot(),DatabaseModule, AdherentModule, AbonnementModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
