import { Injectable } from '@nestjs/common';
import {AdherentDto} from "./dto/adherent.dto";
import {Adherent} from "../entities/adherent.entity";
import {Abonnement} from "../entities/abonnement.entity";
import {IsString} from "class-validator";
import {AbonnementService} from "../abonnement/abonnement.service";
import {RedisService} from "@mistercoookie/redis-pub-sub";

@Injectable()
export class AdherentService {

    constructor(private abonnementService: AbonnementService) {
    }



    async getAdherent(){
        return Adherent.find()
    }

    async getOneAdherent(id: string){
        return Adherent.findOne({
            where: {
                id: id
            }
        })
    }
    async createAdherent(body: AdherentDto){
       const adherent = new Adherent()
       adherent.firstname = body.firstname
       adherent.lastname = body.lastname
       adherent.email = body.email
       adherent.phone= body.phone

        const abonnement = await this.abonnementService.createAbonnement(adherent.id)

        RedisService.publish('g2-create-adherant', adherent)
        return adherent.save()
    }
}
