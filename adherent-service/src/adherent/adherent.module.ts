import { Module } from '@nestjs/common';
import { AdherentController } from './adherent.controller';
import { AdherentService } from './adherent.service';
import {AbonnementService} from "../abonnement/abonnement.service";

@Module({
  controllers: [AdherentController],
  providers: [AdherentService, AbonnementService]
})
export class AdherentModule {}
