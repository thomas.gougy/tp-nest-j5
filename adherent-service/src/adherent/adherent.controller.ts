import {Body, Controller, Get, Param, Post, UseGuards} from '@nestjs/common';
import {AdherentDto} from "./dto/adherent.dto";
import {AdherentService} from "./adherent.service";
import {ServiceAuthGuard} from "../guard/auth.guard";

@Controller('adherent')
export class AdherentController {

    constructor(private adherentService: AdherentService) {
    }

    @UseGuards(ServiceAuthGuard)
    @Get()
    getAllAdherent(){
        return this.adherentService.getAdherent()
    }
    @UseGuards(ServiceAuthGuard)
    @Get("/:id")
    getOneAdherant(@Param('id')adherantId: string){
        return this.adherentService.getOneAdherent(adherantId)
    }
    @UseGuards(ServiceAuthGuard)
    @Post()
    createAdherent(@Body()body: AdherentDto){
        return this.adherentService.createAdherent(body)

    }
}
