import {IsString} from "class-validator";


export class AdherentDto{

    @IsString()
    firstname: string

    @IsString()
    lastname : string

    @IsString()
    email: string

    @IsString()
    phone: string

}