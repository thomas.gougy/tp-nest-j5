import {IsString} from "class-validator";
import {AbonnementTypeEnum} from "../../enum/abonnementType.enum";


export class AbonnementDto{
    @IsString()
    adherentId: string

    @IsString()
    abonnementTypeId: string = AbonnementTypeEnum.DECOUVERTE
}

export class AbonnementUpdateDto{
    @IsString()
    abonnementTypeId: string

    @IsString()
    adherentId: string

}