import { Injectable } from '@nestjs/common';
import {Abonnement} from "../entities/abonnement.entity";
import {AbonnementTypeEnum} from "../enum/abonnementType.enum";
import {AbonnementType} from "../entities/abonnementType.entity";
import {AbonnementDto, AbonnementUpdateDto} from "./dto/abonnementDto";
import {RedisService} from "@mistercoookie/redis-pub-sub";

@Injectable()
export class AbonnementService {


    async createAbonnement(adherentId: string, abonnementTypeId : string = AbonnementTypeEnum.DECOUVERTE){
        const abonnement = new Abonnement()
        abonnement.abonnementType = abonnementTypeId
        abonnement.adherent = adherentId

        return abonnement.save()
    }

    async modifyAbonnement(abonnementId:string, body: AbonnementUpdateDto ){
        const oldAbonnement = await Abonnement.findOne({
            where:{
                id: abonnementId
            }})

        oldAbonnement.dateFin = new Date()
        oldAbonnement.save()

        const  abonnement = new Abonnement()
        abonnement.adherent = body.adherentId
        abonnement.abonnementType = body.abonnementTypeId

        RedisService.publish('g2-update-abonnement', abonnement)
        return abonnement.save()

    }
}
