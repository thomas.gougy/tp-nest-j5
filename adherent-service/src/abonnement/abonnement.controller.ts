import {Body, Controller, Param, Post, Put, UseGuards} from '@nestjs/common';
import {AbonnementService} from "./abonnement.service";
import {AbonnementDto, AbonnementUpdateDto} from "./dto/abonnementDto";
import {ServiceAuthGuard} from "../guard/auth.guard";

@Controller('abonnement')
export class AbonnementController {
    constructor(private abonnementService: AbonnementService) {

    }

    @UseGuards(ServiceAuthGuard)
    @Post()
    createAbonnement(@Body()body:AbonnementDto){
        return this.abonnementService.createAbonnement(body.adherentId)
    }
    @UseGuards(ServiceAuthGuard)
    @Put('/:id')
    updateAbonnement(@Param('id')aboId: string ,@Body()body: AbonnementUpdateDto){
        return this.abonnementService.modifyAbonnement(aboId, body)

    }

}
