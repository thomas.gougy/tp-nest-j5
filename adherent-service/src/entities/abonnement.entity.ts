import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {AbonnementType} from "./abonnementType.entity";
import {Adherent} from "./adherent.entity";


@Entity()
export class Abonnement extends BaseEntity{
    @PrimaryGeneratedColumn('uuid')
    id: string

    @ManyToOne(()=>AbonnementType, (type)=> type, {eager: true})
    abonnementType: string

    @Column('date',{default: new Date()})
    dateDebut: Date

    @Column('date', {nullable: true, default: null})
    dateFin: Date

    @ManyToOne(()=> Adherent, (adherent)=> adherent.abonnements)
    adherent: string
}






