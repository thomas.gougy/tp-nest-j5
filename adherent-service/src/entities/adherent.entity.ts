import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Abonnement} from "./abonnement.entity";
import {Emprunt} from "./emprunt.entity";


@Entity()
export class Adherent extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column('varchar')
    firstname: string

    @Column('varchar')
    lastname: string

    @Column('varchar')
    email: string

    @Column('varchar')
    phone: string

    @OneToMany(()=> Abonnement, (abonnement)=> abonnement.adherent)
    abonnements: Abonnement[]

    @OneToMany(()=> Emprunt, (emprunt) => emprunt.adherent)
    emprunts: Emprunt[];

}