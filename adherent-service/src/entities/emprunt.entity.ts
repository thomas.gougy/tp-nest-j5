import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Adherent } from "./adherent.entity";
import { Media } from "./media.entity";

@Entity()
export class Emprunt extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column('date', {default: new Date()})
    dateEmprunt: Date

    @Column('date')
    dateFinale: Date

    @Column('date')
    dateRendu: Date

    @Column('boolean')
    isChecked: Boolean

    @ManyToOne(() => Media, (media) => media.emprunts, {eager: true})
    media: Media

    @ManyToOne(() => Adherent, (adherent) => adherent.emprunts, {eager: true})
    adherent: Adherent
}
