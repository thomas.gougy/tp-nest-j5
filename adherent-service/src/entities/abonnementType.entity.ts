import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity()
export class AbonnementType extends BaseEntity{

    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column('varchar')
    name: string

    @Column('int')
    nbEmpruntMax

    @Column('int')
    tempsMax: number

    @Column('int', {nullable: true})
    conditionUp: number

    @Column('int', {nullable: true})
    conditionDown: number



}